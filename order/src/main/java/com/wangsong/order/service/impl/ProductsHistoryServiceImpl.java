package com.wangsong.order.service.impl;

import com.wangsong.order.entity.ProductsHistory;
import com.wangsong.order.mapper.ProductsHistoryMapper;
import com.wangsong.order.service.IProductsHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2021-09-25
 */
@Service
public class ProductsHistoryServiceImpl extends ServiceImpl<ProductsHistoryMapper, ProductsHistory> implements IProductsHistoryService {

}
