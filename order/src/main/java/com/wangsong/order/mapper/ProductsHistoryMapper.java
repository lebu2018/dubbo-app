package com.wangsong.order.mapper;

import com.wangsong.order.entity.ProductsHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2021-09-25
 */
public interface ProductsHistoryMapper extends BaseMapper<ProductsHistory> {

}
