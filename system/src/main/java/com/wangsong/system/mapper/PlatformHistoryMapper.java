package com.wangsong.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wangsong.system.entity.PlatformHistory;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2021-10-01
 */
public interface PlatformHistoryMapper extends BaseMapper<PlatformHistory> {

}
