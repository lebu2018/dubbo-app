package com.wangsong.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wangsong.system.entity.PlatformHistory;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2021-10-01
 */
public interface IPlatformHistoryService extends IService<PlatformHistory> {

}
