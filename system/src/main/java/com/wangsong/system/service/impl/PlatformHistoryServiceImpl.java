package com.wangsong.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wangsong.system.entity.PlatformHistory;
import com.wangsong.system.mapper.PlatformHistoryMapper;
import com.wangsong.system.service.IPlatformHistoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2021-10-01
 */
@Service
public class PlatformHistoryServiceImpl extends ServiceImpl<PlatformHistoryMapper, PlatformHistory> implements IPlatformHistoryService {

}
