package com.wangsong.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wangsong.system.entity.PlatformAmount;
import com.wangsong.system.mapper.PlatformAmountMapper;
import com.wangsong.system.service.IPlatformAmountService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2021-10-01
 */
@Service
public class PlatformAmountServiceImpl extends ServiceImpl<PlatformAmountMapper, PlatformAmount> implements IPlatformAmountService {

}
